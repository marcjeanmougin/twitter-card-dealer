#!/usr/bin/python3
from credentials import *
import tweepy
import json

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
myFollowers=set()
for page in tweepy.Cursor(api.followers_ids, id=902685489567858688).pages():
    for f in page:
        myFollowers.add(f)
        print(f)
myFollowers.add(902685489567858688)


class MyEventListener(tweepy.StreamListener):
    def on_friends(self, status):
        print(status)
        for i in status:
            myFollowers.add(i)
    def on_event(self, event):
        print(event)
        if(event.event=="follow"):
            myFollowers.add(event.source['id'])
    def on_status(self, status):
        print(status)
        for u in status.entities['user_mentions']:
            if(u['id'] not in myFollowers):
                return
        for u in status.entities['user_mentions']:
            api.send_direct_message(user_id=u['id'],text="lala")

    def on_error(self, status_code):
        if status_code == 420:
            return False

events = tweepy.Stream(auth=api.auth, listener = MyEventListener())
events.userstream(replies=all)
